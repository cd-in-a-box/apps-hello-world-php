FROM php:7.2-apache

RUN apt-get install -y \
        libzip-dev \
        zip \
  && docker-php-ext-configure zip --with-libzip \
  && docker-php-ext-install zip \
  && apt-get clean autoclean && apt-get autoremove --yes \
  && rm -rf /var/lib/{apt,dpkg,cache,log}/

COPY --from=composer:latest /usr/bin/composer /usr/local/bin/composer

COPY src/ /var/www/html/
